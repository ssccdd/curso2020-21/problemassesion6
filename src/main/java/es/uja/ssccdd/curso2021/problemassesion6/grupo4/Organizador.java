/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.RecursiveAction;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo4.Utils.TRABAJO_MINIMO;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Organizador extends RecursiveAction {

    private final ArrayList<Plato> platos;
    private final int inicioTrabajo; //Inclusivo, se usa para el trabajo.
    private final int finTrabajo; //Esclusivo, no se usa

    public Organizador(ArrayList<Plato> platos, int inicioTrabajo, int finTrabajo) {
        this.platos = platos;
        this.inicioTrabajo = inicioTrabajo;
        this.finTrabajo = finTrabajo;
    }

    @Override
    protected void compute() {

        int tamañoTrabajo = finTrabajo - inicioTrabajo;

        if (tamañoTrabajo == TRABAJO_MINIMO) {

            if (platos.get(inicioTrabajo).getiD() > platos.get(finTrabajo - 1).getiD()) {
                Collections.swap(platos, inicioTrabajo, finTrabajo - 1);
            }

        } else {

            Organizador org1 = new Organizador(platos, inicioTrabajo, inicioTrabajo + (tamañoTrabajo / 2));
            Organizador org2 = new Organizador(platos, inicioTrabajo + (tamañoTrabajo / 2), finTrabajo);

            invokeAll(org1, org2);

            reorganizarSubTrabajos();

        }
    }

    private void reorganizarSubTrabajos() {

        int indiceInicio = inicioTrabajo;
        int indiceFinal = finTrabajo;

        ArrayList<Plato> aux = new ArrayList<>(platos.subList(indiceInicio, indiceFinal));
        
        Collections.sort(aux, (o1, o2) -> {
            return o1.getiD()- o2.getiD();
        });

        for (int i = 0; i < aux.size(); i++) {
            platos.set(indiceInicio + i, aux.get(i));
        }
        
    }

    private void reorganizarSubTrabajosManual() {

        int indiceInicio = inicioTrabajo;
        int indiceMedio = inicioTrabajo + ((finTrabajo - inicioTrabajo) / 2);
        int indiceFinal = finTrabajo;

        ArrayList<Plato> auxA = new ArrayList<>(platos.subList(indiceInicio, indiceMedio));
        ArrayList<Plato> auxB = new ArrayList<>(platos.subList(indiceMedio, indiceFinal));

        int indiceInsercion = indiceInicio;

        while (auxA.size() > 0 && auxB.size() > 0) {

            if (auxA.get(0).getiD() < auxB.get(0).getiD()) {
                platos.set(indiceInsercion++, auxA.remove(0));
            } else {
                platos.set(indiceInsercion++, auxB.remove(0));
            }
        }

        while (auxA.size() > 0) {
            platos.set(indiceInsercion++, auxA.remove(0));
        }

        while (auxB.size() > 0) {
            platos.set(indiceInsercion++, auxB.remove(0));
        }

    }

}
