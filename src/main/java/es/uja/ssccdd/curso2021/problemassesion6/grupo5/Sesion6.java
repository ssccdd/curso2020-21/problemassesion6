/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo5.Utils.MODELOS_A_GENERAR;

import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion6 {

    public static void main(String[] args) throws InterruptedException {

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando cliente");

        Cliente cliente = new Cliente();
        ArrayList<Modelo> modelos = cliente.generarModelos(MODELOS_A_GENERAR);

        System.out.println("HILO-Principal Estado Inicial:");
        for (Modelo modelo : modelos) {
            System.out.println(modelo);
        }

        System.out.println("HILO-Principal Generando organizador");

        Organizador organizador = new Organizador(modelos, 0, MODELOS_A_GENERAR);
        ForkJoinPool pool = new ForkJoinPool();
        
        pool.execute(organizador);
        
        System.out.println("HILO-Principal Espera a los organizadores");
        
        pool.shutdown();
        
        try {
            pool.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion6.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("HILO-Principal Resultado ordenación:");
        for (Modelo modelo : modelos) {
            System.out.println(modelo);
        }
    }

}
