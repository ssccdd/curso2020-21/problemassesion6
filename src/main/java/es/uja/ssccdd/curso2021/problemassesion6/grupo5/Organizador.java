/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.RecursiveAction;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo5.Utils.TRABAJO_MINIMO;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Organizador extends RecursiveAction {

    private final ArrayList<Modelo> modelos;
    private final int inicioTrabajo; //Inclusivo, se usa para el trabajo.
    private final int finTrabajo; //Esclusivo, no se usa

    public Organizador(ArrayList<Modelo> modelos, int inicioTrabajo, int finTrabajo) {
        this.modelos = modelos;
        this.inicioTrabajo = inicioTrabajo;
        this.finTrabajo = finTrabajo;
    }

    @Override
    protected void compute() {

        int tamañoTrabajo = finTrabajo - inicioTrabajo;

        if (tamañoTrabajo == TRABAJO_MINIMO) {

            if (modelos.get(inicioTrabajo).getCalidadRequeridad().ordinal()  > modelos.get(finTrabajo - 1).getCalidadRequeridad().ordinal() ) {
                Collections.swap(modelos, inicioTrabajo, finTrabajo - 1);
            }

        } else {

            Organizador org1 = new Organizador(modelos, inicioTrabajo, inicioTrabajo + (tamañoTrabajo / 2));
            Organizador org2 = new Organizador(modelos, inicioTrabajo + (tamañoTrabajo / 2), finTrabajo);

            invokeAll(org1, org2);

            reorganizarSubTrabajos();

        }
    }

    private void reorganizarSubTrabajos() {

        int indiceInicio = inicioTrabajo;
        int indiceFinal = finTrabajo;

        ArrayList<Modelo> aux = new ArrayList<>(modelos.subList(indiceInicio, indiceFinal));
        
        Collections.sort(aux, (o1, o2) -> {
            return o1.getCalidadRequeridad().ordinal() - o2.getCalidadRequeridad().ordinal();
        });

        for (int i = 0; i < aux.size(); i++) {
            modelos.set(indiceInicio + i, aux.get(i));
        }
        
    }

    private void reorganizarSubTrabajosManual() {

        int indiceInicio = inicioTrabajo;
        int indiceMedio = inicioTrabajo + ((finTrabajo - inicioTrabajo) / 2);
        int indiceFinal = finTrabajo;

        ArrayList<Modelo> auxA = new ArrayList<>(modelos.subList(indiceInicio, indiceMedio));
        ArrayList<Modelo> auxB = new ArrayList<>(modelos.subList(indiceMedio, indiceFinal));

        int indiceInsercion = indiceInicio;

        while (auxA.size() > 0 && auxB.size() > 0) {

            if (auxA.get(0).getCalidadRequeridad().ordinal()  < auxB.get(0).getCalidadRequeridad().ordinal() ) {
                modelos.set(indiceInsercion++, auxA.remove(0));
            } else {
                modelos.set(indiceInsercion++, auxB.remove(0));
            }
        }

        while (auxA.size() > 0) {
            modelos.set(indiceInsercion++, auxA.remove(0));
        }

        while (auxB.size() > 0) {
            modelos.set(indiceInsercion++, auxB.remove(0));
        }

    }

}
