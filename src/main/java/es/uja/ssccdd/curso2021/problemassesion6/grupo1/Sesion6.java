/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo1.Constantes.INICIO;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo1.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo1.Constantes.N_DATOS;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo1.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo1.Constantes.aleatorio;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo1.Constantes.VALOR_PRESENTACION;

/**
 *
 * @author pedroj
 */
public class Sesion6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        System.out.println("Empieza la ejecución del hilo principal");
        
        // Creamos el array de datos
        int[] datos = new int[N_DATOS];
        for (int i=0; i < datos.length; i++) {
            datos[i] = aleatorio.nextInt(MAXIMO) + MINIMO;
        }
        
        // Generamos valor1 y valor2
        int valor1 = aleatorio.nextInt(MAXIMO) + MINIMO;
        int valor2 = aleatorio.nextInt(MAXIMO) + MINIMO;
        
        // Presentamos por pantalla el array original, valor1 y valor2
        System.out.println("Hilo principal: El array original de datos es:");
        for (int i=0; i < datos.length; i++) {
            if ( ((i+1) % VALOR_PRESENTACION == 0) || (i == (datos.length - 1)) ) {
                System.out.println(datos[i]);
            } else {
                System.out.print(datos[i] + " ");
            }
        }
        System.out.println("Hilo principal: El valor1 es: " + valor1 +
                " el valor2 es: " + valor2);
        
        
        // Creamos la tarea y el marco de ejecución donde la lanzamos
        Tarea tarea = new Tarea(datos, INICIO, datos.length, valor1, valor2);
        
        ForkJoinPool pool = new ForkJoinPool();
        pool.execute(tarea);
        
        // Esperamos a la finalización de la tarea
        pool.awaitQuiescence(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Presentamos los cambios realizados por la tarea
        System.out.println("Hilo principal: El array modificado de datos es:");
        for (int i=0; i < datos.length; i++) {
            if ( ((i+1) % VALOR_PRESENTACION == 0) || (i == (datos.length - 1)) ) {
                System.out.println(datos[i]);
            } else {
                System.out.print(datos[i] + " ");
            }
        }
        System.out.println("Hilo principal: El valor1 es: " + valor1 +
                " el valor2 es: " + valor2);
        Resultado resultado = tarea.get();
        System.out.println(resultado);
        
        System.out.println("Finaliza la ejecución del hilo principal");
    } 
}
