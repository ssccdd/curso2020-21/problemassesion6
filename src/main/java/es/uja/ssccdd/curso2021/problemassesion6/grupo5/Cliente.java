/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo5.Utils.VALOR_GENERACION;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Cliente {

  public ArrayList<Modelo> generarModelos(int cantidad) {

        ArrayList<Modelo> modelos = new ArrayList<>();

        for (int i = 0; i < cantidad; i++) {
            modelos.add(new Modelo(i, CalidadImpresion.getCalidad(random.nextInt(VALOR_GENERACION))));
        }

        Collections.shuffle(modelos);
        return modelos;
    }

}
