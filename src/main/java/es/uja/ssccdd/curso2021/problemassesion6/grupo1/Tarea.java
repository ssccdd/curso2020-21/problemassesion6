/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo1.Constantes.UMBRAL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pedroj
 */
public class Tarea extends RecursiveTask<Resultado> {
    // Versión UID porque implementa la interface Serializable
    private static final long serialVersionUID = 1L; 
    
    private final int[] datos;
    private final int indiceInicio;
    private final int indiceFinal;
    private final int valor1;
    private final int valor2;
    
    public Tarea(int[] datos, int indiceInicio, int indiceFin, int valor1, int valor2) {
        this.datos = datos;
        this.indiceInicio = indiceInicio;
        this.indiceFinal = indiceFin;
        this.valor1 = valor1;
        this.valor2 = valor2;
    }

    @Override
    protected Resultado compute() {
        Resultado resultado = new Resultado();
        // Comprobamos si podemos resolver nuestra tarea y si no la dividimos en dos
        if (indiceFinal - indiceInicio > UMBRAL) {
            // Dividimos la tarea
            int mitad = (indiceInicio + indiceFinal) / 2;
            System.out.println("Tarea: Tareas pendientes de finalización: " + getQueuedTaskCount());
            Tarea tarea1 = new Tarea(datos, indiceInicio, mitad, valor1, valor2);
            Tarea tarea2 = new Tarea(datos, mitad, indiceFinal, valor1, valor2);
            invokeAll(tarea1, tarea2);
            try {
                // Sumamos los cambios que han realizado las tareas
                resultado.add(tarea1.get(), tarea2.get());
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(Tarea.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // Resolvemos el caso base
            for (int i = indiceInicio; i < indiceFinal; i++) {
                if (datos[i] == valor1) {
                    datos[i] = valor2;
                    // Acumulamos los cambios producidos
                    resultado.add();
                }
            }
        }
        // Devolvemos los cambios realizados por la tarea
        return resultado;
    } 
}
