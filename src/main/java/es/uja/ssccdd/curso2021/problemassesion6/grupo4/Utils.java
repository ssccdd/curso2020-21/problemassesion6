/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo4;

import java.util.Random;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    public static Random random = new Random();

    // Constantes del problema
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_TIPOS_PLATOS = TipoPlato.values().length;
    public static final int PLATOS_A_GENERAR = 128;
    public static final int TRABAJO_MINIMO = 2;

    //Enumerado para el tipo de plato
    public enum TipoPlato {
        PRINCIPAL(50), SEGUNDO(75), POSTRE(100);

        private final int valor;

        private TipoPlato(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un plato relacionado con su valor de generación
         *
         * @param valor, entre 0 y 100, de generación del plato
         * @return el TipoPlato con el valor de generación
         */
        public static TipoPlato getPlatoAleatorio(int valor) {
            TipoPlato resultado = null;
            TipoPlato[] platos = TipoPlato.values();
            int i = 0;

            while ((i < platos.length) && (resultado == null)) {
                if (platos[i].valor >= valor) {
                    resultado = platos[i];
                }

                i++;
            }

            return resultado;
        }
        
        /**
         * Obtenemos un plato relacionado con su valor de generación
         *
         * @param ordinal, entre 0 y TOTAL_TIPO_PLATOS - 1
         * @return el TipoPlato con el valor de generación
         */
        public static TipoPlato getPlatoOrdinal(int ordinal) {
            return TipoPlato.values()[ordinal];
        }
    }

}
