/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo6.Utils.TRABAJO_MINIMO;
import java.util.ArrayList;
import java.util.concurrent.RecursiveAction;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Filtrador extends RecursiveAction {

    private final ArrayList<DosisVacuna> dosis;
    private final int inicioTrabajo; //Inclusivo, se usa para el trabajo.
    private final int finTrabajo; //Esclusivo, no se usa

    public Filtrador(ArrayList<DosisVacuna> dosis, int inicioTrabajo, int finTrabajo) {
        this.dosis = dosis;
        this.inicioTrabajo = inicioTrabajo;
        this.finTrabajo = finTrabajo;
    }

    @Override
    protected void compute() {

        int tamañoTrabajo = finTrabajo - inicioTrabajo;
        int tamañoRestante = tamañoTrabajo - TRABAJO_MINIMO;
        int inicioTrabajoRestante = inicioTrabajo + TRABAJO_MINIMO;

        if (tamañoRestante > TRABAJO_MINIMO) {

            Filtrador org1 = new Filtrador(dosis, inicioTrabajoRestante, inicioTrabajoRestante + (tamañoRestante / 2));
            Filtrador org2 = new Filtrador(dosis, inicioTrabajoRestante + (tamañoRestante / 2), finTrabajo);
            org1.fork();
            org2.fork();

        } else if (tamañoRestante > 0) { //el restante estará entre 0 y 10

            Filtrador org = new Filtrador(dosis, inicioTrabajoRestante, finTrabajo);
            org.fork();

        }
        
        procesarTrabajo();

    }

    private void procesarTrabajo() {

        int tamañoTrabajo = finTrabajo - inicioTrabajo;
        
        if (tamañoTrabajo > TRABAJO_MINIMO) {
            tamañoTrabajo = TRABAJO_MINIMO;
        }

        for (int i = 0; i < tamañoTrabajo; i++) {

            DosisVacuna auxDosis = dosis.get(inicioTrabajo + i);

            if (auxDosis.getiD() % 2 == 0 && auxDosis.getFabricante() == FabricanteVacuna.ASTROLUNAR) {
                auxDosis.anularDosis();

            } else if (auxDosis.getiD() % 3 == 0 && auxDosis.getFabricante() == FabricanteVacuna.ANTIGUA) {
                auxDosis.anularDosis();

            } else if (auxDosis.getiD() % 5 == 0 && auxDosis.getFabricante() == FabricanteVacuna.PFINOS) {
                auxDosis.anularDosis();
            }

        }

    }

}
