/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo6;

import es.uja.ssccdd.curso2021.problemassesion6.grupo6.Utils.FabricanteVacuna;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class DosisVacuna {

    private final int iD;
    private final FabricanteVacuna fabricante;
    private boolean anulada;

    public DosisVacuna(int iD, FabricanteVacuna fabricante) {
        this.iD = iD;
        this.fabricante = fabricante;
        anulada = false;
    }

    public int getiD() {
        return iD;
    }

    public FabricanteVacuna getFabricante() {
        return fabricante;
    }

    public void anularDosis() {
        this.anulada = true;
    }

    public boolean isAnulada() {
        return anulada;
    }
    
    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        return "Vacuna{" + (anulada?"ANULADA ":"") + "iD= " + iD + ", fabricante= " + fabricante + "}";
    }

}
