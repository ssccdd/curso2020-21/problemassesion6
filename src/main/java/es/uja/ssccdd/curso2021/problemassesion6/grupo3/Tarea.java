/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo2.Constantes.UMBRAL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pedroj
 */
public class Tarea extends RecursiveTask<Resultado> {
    // Versión UID porque implementa la interface Serializable
    private static final long serialVersionUID = 1L; 
    
    private final int[] datos;
    private final int indiceInicio;
    private final int indiceFinal;
    private final int valor;
    
    public Tarea(int[] datos, int indiceInicio, int indiceFin, int valor) {
        this.datos = datos;
        this.indiceInicio = indiceInicio;
        this.indiceFinal = indiceFin;
        this.valor = valor;
    }

    @Override
    protected Resultado compute() {
        Resultado resultado = new Resultado();
        // Comprobamos si podemos resolver nuestra tarea y si no la dividimos en dos
        if (indiceFinal - indiceInicio > UMBRAL) {
            // Dividimos la tarea
            int mitad = (indiceInicio + indiceFinal) / 2;
            System.out.println("Tarea: Tareas pendientes de finalización: " + getQueuedTaskCount());
            Tarea tarea1 = new Tarea(datos, indiceInicio, mitad, valor);
            Tarea tarea2 = new Tarea(datos, mitad, indiceFinal, valor);
            invokeAll(tarea1, tarea2);
            try {
                // Sumamos los cambios que han realizado las tareas
                resultado.add(tarea1.get(), tarea2.get());
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(Tarea.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // Resolvemos el caso base
            if ( valor % 2 == 0) {
                for (int i = indiceInicio; i < indiceFinal; i++) {
                    if ((datos[i] % 2 == 0) && (datos[i] != valor)) {
                        // No realizamos el cambio si es igual al valor
                        datos[i] = valor;
                        // Acumulamos los cambios realizados
                        resultado.add();
                    }
                }
            } else {
                for (int i = indiceInicio; i < indiceFinal; i++) {
                    if ((datos[i] % 2 != 0) && (datos[i] != valor)) {
                        // No realizamos el cambio si es igual al valor
                        datos[i] = valor;
                        // Acumulamos los cambios realizados
                        resultado.add();
                    }
                }
            }
        }
        // Devolvemos los cambios realizados por la tarea
        return resultado;
    }
}
