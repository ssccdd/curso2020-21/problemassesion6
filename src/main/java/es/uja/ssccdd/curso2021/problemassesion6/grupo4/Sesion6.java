/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo4.Utils.PLATOS_A_GENERAR;
import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion6 {

    public static void main(String[] args) {
        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando restaurante");

        Restaurante restaurante = new Restaurante();
        ArrayList<Plato> platos = restaurante.generarPlatos(PLATOS_A_GENERAR);

        System.out.println("HILO-Principal Estado Inicial:");
        for (Plato plato : platos) {
            System.out.println(plato);
        }

        System.out.println("HILO-Principal Generando organizador");

        Organizador organizador = new Organizador(platos, 0, PLATOS_A_GENERAR);
        ForkJoinPool pool = new ForkJoinPool();
        
        pool.execute(organizador);
        
        System.out.println("HILO-Principal Espera a los organizadores");
        
        pool.shutdown();
        
        try {
            pool.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion6.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("HILO-Principal Resultado ordenación:");
        for (Plato plato : platos) {
            System.out.println(plato);
        }

    }

}
