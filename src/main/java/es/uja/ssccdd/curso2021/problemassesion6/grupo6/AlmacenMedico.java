/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo5.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo6.Utils.ID_DOSIS_MAX;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo6.Utils.ID_DOSIS_MIN;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo6.Utils.FabricanteVacuna;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class AlmacenMedico {

    public ArrayList<DosisVacuna> generarDosisVacuna(int cantidad) {

        ArrayList<DosisVacuna> dosis = new ArrayList<>();

        for (int i = 0; i < cantidad; i++) {
            dosis.add(new DosisVacuna(random.nextInt(ID_DOSIS_MAX - ID_DOSIS_MIN) + ID_DOSIS_MIN, FabricanteVacuna.getFabricanteAleatorio(random.nextInt(VALOR_GENERACION))));
        }

        return dosis;
    }

}
