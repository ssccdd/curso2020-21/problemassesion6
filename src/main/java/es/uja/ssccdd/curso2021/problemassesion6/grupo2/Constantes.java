/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo2;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    public static final Random aleatorio = new Random();
    
    public static final int N_DATOS = 10000;
    public static final int VALOR_PRESENTACION = 50;
    public static final int INICIO = 0;
    public static final int MINIMO = 1;
    public static final int MAXIMO = 9;
    public static final int TIEMPO_ESPERA = 1;
    public static final int UMBRAL = 10;
}
