/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo4;

import java.util.ArrayList;
import java.util.Collections;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo4.Utils.TOTAL_TIPOS_PLATOS;
import static es.uja.ssccdd.curso2021.problemassesion6.grupo4.Utils.TipoPlato;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Restaurante {

    public ArrayList<Plato> generarPlatos(int cantidad) {

        ArrayList<Plato> platos = new ArrayList<>();

        for (int i = 0; i < cantidad; i++) {
            platos.add(new Plato(i, TipoPlato.getPlatoOrdinal(i % TOTAL_TIPOS_PLATOS)));
        }

        Collections.shuffle(platos);
        return platos;
    }

}
