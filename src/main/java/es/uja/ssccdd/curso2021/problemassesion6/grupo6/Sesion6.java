/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion6.grupo6.Utils.DOSIS_A_GENERAR;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion6 {

    public static void main(String[] args) {

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando almacén");

        AlmacenMedico almacen = new AlmacenMedico();
        ArrayList<DosisVacuna> dosis = almacen.generarDosisVacuna(DOSIS_A_GENERAR);

        System.out.println("HILO-Principal Estado Inicial:");
        for (DosisVacuna d : dosis) {

            System.out.println(d);

        }

        System.out.println("HILO-Principal Generando organizador");

        Filtrador filtrador = new Filtrador(dosis, 0, DOSIS_A_GENERAR);
        ForkJoinPool pool = new ForkJoinPool();

        pool.execute(filtrador);

        System.out.println("HILO-Principal Espera a los filtradores");

        pool.shutdown();

        try {
            pool.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion6.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("HILO-Principal Resultado ordenación:");
        for (DosisVacuna d : dosis) {

            System.out.println(d);

        }

    }

}
