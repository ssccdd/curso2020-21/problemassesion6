/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion6.grupo3;

/**
 *
 * @author pedroj
 */
public class Resultado {
    private int valor;

    public Resultado(int valor) {
        this.valor = valor;
    }
    
    public Resultado() {
        this.valor = 0;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }
    
    public void add() {
        this.valor++;
    }
    
    public void add(Resultado resultado1, Resultado resultado2) {
        this.valor = resultado1.getValor() + resultado2.getValor();
    }

    @Override
    public String toString() {
        return "Se han completado " + valor + " intercambios";
    }
}
