﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 6

Problemas propuestos para la Sesión 6 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

En esta sesión de prácticas se proponen ejercicio para trabajar con el nuevo **marco de ejecución Fork/Join**. Para ello utilizaremos las clases [`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html), para el **marco de ejecución**, y [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html), para las tareas que se ejecutarán. Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion6#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion6#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion6#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion6#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion6#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion6#grupo-6)

### Grupo 1
 Para la realización de la tarea de cálculo definiremos una clase que hereda de [`RecursiveTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html) que se ejecutará en un **marco Fork/Join** ([`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html)). Estas dos clases son las únicas que utilizaremos para la solución concurrente del ejercicio. 

Hay que completar la implementación de:

 - `Constantes` : Será una interface donde se incluirán las constantes necesarias para la realización del ejercicio.

- `Resultado` : Será la clase que representa el valor que calcula la tarea.
 
 - `Tarea` : Deberá realizar la siguiente acción:
	 - Tendrá un array de números entre 1 y 9. Y dos valores: `valor_1` y `valor_2`. Que también son números entre 1 y 9.
	 - Si el tamaño del array es mayor que 10 se dividirá la tarea en 2 y esperará hasta que finalicen.
	 - La acción para la tarea base es la siguiente:
		 - Buscará números que coincidan con el `valor_1` y los cambiará por el `valor_2`.

- `Hilo principal` : Realizará los siguientes pasos:
	- Creará un array aleatorio de 10000 números entre 1 y 9.
	- También generará dos números entre 1 y 9 para `valor_1` y `valor_2`.
	- Creará un objeto de la clase `Tarea` y lo ejecutará en `ForkJoinPool`.
	- Antes de que se ejecute la tarea mostrará en pantalla el array original, el `valor_1` y `valor_2`.
	- Esperará a que termine la tarea y mostrará el resultado del cambio.

### Grupo 2
Para la realización de la tarea de cálculo definiremos una clase que hereda de [`RecursiveAction`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveAction.html) que se ejecutará en un **marco Fork/Join** ([`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html)). Estas dos clases son las únicas que utilizaremos para la solución concurrente del ejercicio. 

Hay que completar la implementación de:

 - `Constantes` : Será una interface donde se incluirán las constantes necesarias para la realización del ejercicio.
 
 - `Tarea` : Deberá realizar la siguiente acción:
	 - Tendrá un array de números entre 1 y 9. Y dos valores: `valor_1` y `valor_2`. Que también son números entre 1 y 9.
	 - Si el tamaño del array es mayor que 10 se dividirá la tarea en 2 y esperará hasta que finalicen.
	 - La acción para la tarea base es la siguiente:
		 - Buscará números que coincidan con el `valor_1` y los cambiará por el `valor_2`.

- `Hilo principal` : Realizará los siguientes pasos:
	- Creará un array aleatorio de 10000 números entre 1 y 9.
	- También generará dos números entre 1 y 9 para `valor_1` y `valor_2`.
	- Creará un objeto de la clase `Tarea` y lo ejecutará en `ForkJoinPool`.
	- Antes de que se ejecute la tarea mostrará en pantalla el array original, el `valor_1` y `valor_2`.
	- Esperará a que termine la tarea y mostrará el resultado del cambio.


### Grupo 3
Para la realización de la tarea de cálculo definiremos una clase que hereda de [`RecursiveTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html) que se ejecutará en un **marco Fork/Join** ([`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html)). Estas dos clases son las únicas que utilizaremos para la solución concurrente del ejercicio. 

Hay que completar la implementación de:

 - `Constantes` : Será una interface donde se incluirán las constantes necesarias para la realización del ejercicio.

- `Resultado` : Será la clase que representa el valor que calcula la tarea.
 
 - `Tarea` : Deberá realizar la siguiente acción:
	 - Tendrá un array de números entre 1 y 9. Y un `valor` que será un número entre 1 y 9.
	 - Si el tamaño del array es mayor que 10 se dividirá la tarea en 2 y esperará hasta que finalicen.
	 - La acción para la tarea base es la siguiente:
		 - Si `valor` es par cambiará los elementos pares del array por ese `valor`. Análogamente para el caso que sea impar.

- `Hilo principal` : Realizará los siguientes pasos:
	- Creará un array aleatorio de 10000 números entre 1 y 9.
	- También generará un número entre 1 y 9 para `valor`.
	- Creará un objeto de la clase `Tarea` y lo ejecutará en `ForkJoinPool`.
	- Antes de que se ejecute la tarea mostrará en pantalla el array original, el `valor`.
	- Esperará a que termine la tarea y mostrará el resultado del cambio.
	
	
## Grupo 4
Se ha cambiado la forma de producción de platos, ahora una gran cadena de restaurantes nos proporciona todos los platos a la vez, pero por su organización, los platos vienen desordenados. En esta sesión nos vamos a enfocar a la ordenación previa de los platos antes de su uso por los repartidores. Para ello se ha comprado una cadena de producción, que permite la activación o desactivación de diferentes módulos de ordenación (`Organizador`) según la tarea. 
Para esta práctica se proporcionan las siguientes clases:

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. No están todas las constantes definidas.
- `Plato`: Clase que representa a un plato de tipo `TipoPlato`.

El ejercicio consiste en completar la implementación de las siguientes clases:
 - `Restaurante`: Generará tantos platos como se le pidan, con ids consecutivos y repitiendo los tipos de platos de forma cíclica. Ej. el primer plato será un `PRINCIPAL`, el segundo `SEGUNDO`, el tercero `POSTRE` .... el octavo `SEGUNDO`, etc. Después de generar los platos se debe de usar la función `Collections.shuffle(platos)` para que se desordenen.
 - `Organizador`: Esta clase hereda de `RecursiveAction` y será la encargada de la ordenación, siguiendo el esquema recomendado por Java.
	 - Tendrá un ArrayList con los platos, que tendrá que ordenar por ID.
	 - Si el tamaño del trabajo asignado es dos intercambia los platos en caso de estar desordenados.
	 - Si el tamaño del trabajo asignado es mayor a 2 dividirá el trabajo en dos y lanzará dos organizadores de forma síncrona y por último mezclará las soluciones de ambas subtareas.
	 - La tarea de mezcla está ya implementada solo tenéis que indicar el índice de inicio (inclusivo) y el de final (exclusivo) de trabajo.
 - `Hilo Principal`:
	 - Generará un restaurante al que le pedirá unos platos a generar. Para simplificar el ejercicio, este número es potencia de dos.
	 - Imprimirá el estado inicial del array.
	 - Generará un `Organizador` al que le pasará el array de platos, y los índices inicial y final del array.
	 - Esperará a que termine la tarea y mostrará el resultado de la ordenación.

## Grupo 5
Se ha cambiado la forma de producción de modelos, ahora un solo cliente nos proporciona una gran cantidad de modelos a la vez, pero por su organización, los modelos vienen desordenados y es necesario reordenarlos usando el ordinal de `CalidadImpresion`. En esta sesión nos vamos a enfocar a la ordenación previa de los modelos antes de su uso por los ingenieros. Para ello se ha comprado una cadena de producción, que permite la activación o desactivación de diferentes modulos de ordenación (`Organizador`) según la tarea. 

Para esta práctica se proporcionan las siguientes clases:
- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. No están todas las constantes definidas.
- `Modelo`: Clase que representa a un modelo 3D con unos requisitos de tipo `CalidadImpresion`.

El ejercicio consiste en completar la implementación de las siguientes clases:
 - `Cliente`: Generará tantos platos como se le pidan, con ids consecutivos y calidades aleatorias. Después de generar los modelos se debe de usar la función `Collections.shuffle(modelos)` para que se desordenen.
 - `Organizador`: Esta clase hereda de `RecursiveAction` y será la encargada de la ordenación, siguiendo el esquema recomendado por Java.
	 - Tendrá un ArrayList con los modelos, que tendrá que ordenar por el ordinal de `CalidadImpresion`.
	 - Si el tamaño del trabajo asignado es dos intercambia los modelos en caso de estar desordenados.
	 - Si el tamaño del trabajo asignado es mayor a 2 dividirá el trabajo en dos y lanzará dos organizadores de forma síncrona y por último mezclará las soluciones de ambas subtareas.
	 - La tarea de mezcla está ya implementada solo tenéis que indicar el índice de inicio (inclusivo) y el de final (exclusivo) de trabajo.
 - `Hilo Principal`:
	 - Generará un cliente al que le pedirá unos modelos a generar. Para simplificar el ejercicio, este número es potencia de dos.
	 - Imprimirá el estado inicial del array.
	 - Generará un `Organizador` al que le pasará el array de modelos, y los índices inicial y final del array.
	 - Esperará a que termine la tarea y mostrará el resultado de la ordenación.
	

## Grupo 6
Los almacenes generan junto a las dosis de vacunas unos viales de antihistamínicos que reducen los síntomas de posibles reacciones, pero se introducen erróneamente en el sistema generando en la base de datos entradas falsas de dosis de vacunas. En esta sesión nos vamos a enfocar en la búsqueda y anulación de esos viales inexistentes antes de su uso por los Enfermeros. Para ello se ha generado un proceso, que permite autorreplicarse si considera que la tarea es demasiado grande para procesarla en un tiempo razonable. 

Para esta práctica se proporcionan las siguientes clases:
- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. No están todas las constantes definidas.
- `DosisVacuna`: Clase que representa a cada dosis de la vacuna de un `FabricanteVacuna` determinado. Se ha incluido un método para anular la dosis.

El ejercicio consiste en completar la implementación de las siguientes clases:
 - `AlmacenMedico`: Generará tantas dosis como se le pidan, con ids aleatorios entre 10000 y 20000 con un fabricante aleatorio.
 - `Filtrador`: Esta clase hereda de `RecursiveAction` y será la encargada del filtrado, siguiendo el esquema recomendado por Java.
	 - Tendrá un ArrayList con las dosis, que tendrá que filtrar y el trabajo asignado.
	 - Si el tamaño de trabajo es mayor a 10 pero menor de 20, lanzará una subtarea asíncrona con las dosis que pasen de 10.
	 - Si el tamaño de trabajo es mayor a 20, lanzará dos subtareas asíncronas dividiendo las dosis que pasen de 10.
	 - Buscará y anulará las dosis incorrectas entre las diez primeras del trabajo asignado, siguiendo el siguiente esquema:
		 - Si la dosis es de `ASTROLUNAR`, se anularán las dosis con ID par.
		 - Si la dosis es de `ANTIGUA`, se anularán las dosis con ID divisible por 3.
		 - Si la dosis es de `PFINOS`, se anularán las dosis con ID divisible por 5.
 - `Hilo Principal`:
	 - Generará un almacén que creará 150 dosis de vacuna.
	 - Imprimirá el estado inicial del array.
	 - Generará un `Filtrador` al que le pasará el array de modelos, y los índices inicial y final del array.
	 - Esperará a que termine la tarea y mostrará el resultado del filtrado.
